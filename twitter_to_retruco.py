#! /usr/bin/env python3


# Twitter-to-Retruco -- Convert tweets to Retruco's statements.
# By: Emmanuel Raviart <emmanuel@retruco.org>
#
# Copyright (C) 2017 Emmanuel Raviart
# https://framagit.org/retruco/twitter-to-retruco
#
# Twitter-to-Retruco is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Twitter-to-Retruco is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""Convert tweets to Retruco's statements."""


import argparse
import json
import logging
import os
import sys
import urllib.parse

import requests
from requests_oauthlib import OAuth1, OAuth1Session

import config


app_name = os.path.splitext(os.path.basename(__file__))[0]
log = logging.getLogger(app_name)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('discussion_id', help='ID of discussion to add interventions to')
    parser.add_argument('track', help='text to filter in Twitter stream')
    parser.add_argument('language', choices=['en', 'es', 'fr'], help='language of argument to import')
    parser.add_argument('-v', '--verbose', action='store_true', default=False, help='increase output verbosity')
    args = parser.parse_args()

    logging.basicConfig(level=logging.DEBUG if args.verbose else logging.WARNING, stream=sys.stdout)

    auth = OAuth1(
        client_key=config.consumer_key,
        client_secret=config.consumer_secret,
        resource_owner_key=config.access_token,
        resource_owner_secret=config.access_token_secret,
        )
    response = requests.post(
        'https://stream.twitter.com/1.1/statuses/filter.json',
        auth=auth,
        data= dict(
            language=args.language,
            track=args.track,
            ),
        stream=True,
        )

    # Set chunk_size to a very small number if you don't want to wait forever.
    for line in response.iter_lines(chunk_size=1, decode_unicode=True):
        if line:
            data = json.loads(line)
            if isinstance(data.get('id_str'), str) and isinstance(data.get('text'), str):
                if data.get('retweeted_status') is None:
                    text = data.get('extended_tweet', {}).get('full_text') or data['text']
                    print()
                    print('Tweet: {}'.format(text))
                    tweet_id = upsert_text(text)
                    upsert_property(tweet_id, data['lang'], tweet_id)
                    upsert_property(args.discussion_id, 'intervention', tweet_id)

    return 0


def upsert_property(object_id, key_id, value_id):
    response = requests.post(urllib.parse.urljoin(config.api_url, '/properties'),
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Retruco-API-Key': config.api_key,
            },
        json = dict(
            keyId=key_id,
            objectId=object_id,
            rating=1,
            valueId=value_id,
            ),
        )
    if response.status_code not in (requests.codes.created, requests.codes.ok):
        error_body = response.content.decode('utf-8')
        try:
            error_json = json.loads(error_body)
        except json.JSONDecodeError:
            print('Error response {}:\n{}'.format(response.status_code, error_body))
        else:
            print('Error response {}:\n{}'.format(response.status_code, json.dumps(error_json, ensure_ascii = False, indent = 2)))
        response.raise_for_status()
    body = response.json()
    return body['data']['id']


def upsert_text(text):
    text = text.strip()
    assert text

    response = requests.post(urllib.parse.urljoin(config.api_url, '/values'),
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Retruco-API-Key': config.api_key,
            },
        json = dict(
            schema='schema:string',
            value=text,
            widget='widget:input-text',
            ),
        )
    if response.status_code not in (requests.codes.created, requests.codes.ok):
        error_body = response.content.decode('utf-8')
        try:
            error_json = json.loads(error_body)
        except json.JSONDecodeError:
            print('Error response {}:\n{}'.format(response.status_code, error_body))
        else:
            print('Error response {}:\n{}'.format(response.status_code, json.dumps(error_json, ensure_ascii = False, indent = 2)))
        response.raise_for_status()
    body = response.json()
    id = body['data']['id']

    response = requests.post(urllib.parse.urljoin(config.api_url, '/statements/{}/rating'.format(id)),
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Retruco-API-Key': config.api_key,
            },
        json = dict(
            rating=1,
            ),
        )
    if response.status_code not in (requests.codes.created, requests.codes.ok):
        error_body = response.content.decode('utf-8')
        try:
            error_json = json.loads(error_body)
        except json.JSONDecodeError:
            print('Error response {}:\n{}'.format(response.status_code, error_body))
        else:
            print('Error response {}:\n{}'.format(response.status_code, json.dumps(error_json, ensure_ascii = False, indent = 2)))
        response.raise_for_status()

    return id


if __name__ == "__main__":
    sys.exit(main())
