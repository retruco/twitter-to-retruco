# Twitter-to-Retruco

Convert tweets to [Retruco](https://retruco.org/)'s statements.

## Example

```sh
./twitter_to_retruco.py -k SECRET_API_KEY 3871 fr https://api.retruco.org/
```